import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { WaitingRoutingModule } from './waiting-routing.module';
import {MainComponent} from './main/main.component';
import { WaitingComponent } from './waiting.component';
import {RegistersComponent} from './register/register.component';
import { NgZorroModule } from '../../../ng-zorro.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from 'src/app/shared/shared.module';



@NgModule({
  declarations: [
    WaitingComponent,MainComponent,RegistersComponent
  ],
  imports: [
    CommonModule,
    NgZorroModule,
    FormsModule,SharedModule,
    ReactiveFormsModule,
    WaitingRoutingModule
  ]
})
export class WaitingModule { }
