import { Component, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { NzButtonSize } from 'ng-zorro-antd/button';
import { NurseService } from '../../services/nurse.service';
import { DateTime } from 'luxon';
import { NgxSpinnerService, Spinner } from 'ngx-spinner';
import { NotificationService } from '../../../../shared/services/notification.service';
import { AxiosResponse } from 'axios';
import { LibService } from '../../../../shared/services/lib.service';

import {
  ChartComponent,
  ApexAxisChartSeries,
  ApexChart,
  ApexXAxis,
  ApexDataLabels,
  ApexStroke,
  ApexMarkers,
  ApexYAxis,
  ApexGrid,
  ApexTitleSubtitle,
  ApexLegend,
} from 'ng-apexcharts';

export type ChartOptions = {
  series: ApexAxisChartSeries;
  chart: ApexChart;
  xaxis: ApexXAxis;
  stroke: ApexStroke;
  dataLabels: ApexDataLabels;
  markers: ApexMarkers;
  colors: string[];
  yaxis: ApexYAxis;
  grid: ApexGrid;
  legend: ApexLegend;
  title: ApexTitleSubtitle;
};

@Component({
  selector: 'app-nurse-note',
  templateUrl: './nurse-note.component.html',
  styleUrls: ['./nurse-note.component.css'],
})
export class NurseNoteComponent {
  @ViewChild('chart', { static: true }) chart: any;
  public chartOptions: any;
  basicBarChart: any;

  //public
  doctors: any = [];
  doctorname: any;
  //
  checked = true;
  isVisible = false;
  isVisibleVS = false;
  isVisibleVSEdit = false;
  queryParamsData: any;
  size: NzButtonSize = 'default';
  itemPatientInfo: any;
  sessionPatientInfo: any;
  //ken writing
  sessionListnursenote: any;
  listnursenote: any;
  itemActivityInfo: any;
  itemEvaluate: any;
  activity_checked: any = [];
  evaluate_checked: any = [];
  problem_list: any;
  itemNursenote: any;
  getproblem_list: any;
  countrowdata: any;
  itemNursenoteDate: any;
  itemNursenoteTime: any;
  isSaved = false;
  //แสดงข้อมูลด้านบนสุดของแต่ละแท็บ
  topbed: any;
  //Patient info
  address: any;
  admit_id: any;
  age: any;
  an: any;
  blood_group: any;
  cid: any;
  citizenship: any;
  create_by: any;
  create_date: any;
  dob: any;
  fname: any;
  gender: any;
  hn: any;
  id: any;
  inscl: any;
  inscl_name: any;
  insurance_id: any;
  is_active: any;
  is_pregnant: any;
  lname: any;
  married_status: any;
  modify_by: any;
  modify_date: any;
  nationality: any;
  occupation: any;
  phone: any;
  reference_person_address: any;
  reference_person_name: any;
  reference_person_phone: any;
  reference_person_relationship: any;
  religion: any;
  title: any;

  //Nurse V/S info
  body_height: any;
  body_temperature: any;
  body_weight: any;
  chief_complaint: any;
  diatolic_blood_pressure: any;
  eye_score: any;
  movement_score: any;
  oxygen_sat: any;
  past_history: any;
  physical_exam: any;
  present_illness: any;
  pulse_rate: any;
  respiratory_rate: any;
  systolic_blood_pressure: any;
  verbal_score: any;
  waist: any;
  //ken VS input
  rr: any;
  bp: any;
  bw: any;
  bt: any;
  pr: any;
  painscore: any;
  oralfluide: any;
  parenterat: any;
  urineout: any;
  emesis: any;
  drainage: any;
  aspiration: any;
  stools: any;
  urine: any;
  medications: any;

  // Nurse Note
  NurseNote: any = [];
  // Doctor Order
  doctorOrder: any = [];

  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private nurseService: NurseService,
    private spinner: NgxSpinnerService,
    private notificationService: NotificationService,
    private libService: LibService
  ) {
    this.apexChartLine();
    this._basicBarChart(          );
  }
  apexChartLine() {
    this.chartOptions = {
      series: [
        {
          name: 'High - 2013',
          data: [28, 29, 33, 36, 32, 32, 33],
        },
        {
          name: 'Low - 2013',
          data: [12, 11, 14, 18, 17, 13, 13],
        },
      ],
      chart: {
        height: 350,
        width: '80%',
        type: 'line',
        dropShadow: {
          enabled: true,
          color: '#000',
          top: 18,
          left: 7,
          blur: 10,
          opacity: 0.2,
        },
        toolbar: {
          show: true,
        },
      },
      colors: ['#77B6EA', '#545454'],
      dataLabels: {
        enabled: true,
      },
      stroke: {
        curve: 'smooth',
      },
      title: {
        text: 'Average High & Low Temperature',
        align: 'left',
      },
      grid: {
        borderColor: '#e7e7e7',
        row: {
          colors: ['#f3f3f3', 'transparent'], // takes an array which will be repeated on columns
          opacity: 0.5,
        },
      },
      markers: {
        size: 1,
      },
      xaxis: {
        categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul'],
        title: {
          text: 'Month',
        },
      },
      yaxis: {
        title: {
          text: 'Temperature',
        },
        min: 5,
        max: 40,
      },
      legend: {
        position: 'top',
        horizontalAlign: 'right',
        floating: true,
        offsetY: -25,
        offsetX: -5,
      },
    };
  }
  
  /**
   * Basic Bar Chart
   */
  _basicBarChart() {
    // colors = this.getChartColorsArray(colors);
    this.basicBarChart = {
      series: [
        {
          data: [1010, 1640, 490, 1255, 1050, 689, 800, 420, 1085, 589],
          name: 'Sessions',
        },
      ],
      chart: {
        type: 'bar',
        height: 400,
        direction: 'rtl',
        toolbar: {
          show: false,
        },
      },
      plotOptions: {
        bar: {
          borderRadius: 4,
          horizontal: true,
          distributed: true,
          dataLabels: {
            position: 'top',
          },
        },
      },
      dataLabels: {
        enabled: true,
        offsetX: 32,
        style: {
          fontSize: '12px',
          fontWeight: 400,
          colors: ['#adb5bd'],
        },
      },
      // colors: colors,
      colors: ['#77B6EA', '#545454'],
      legend: {
        show: false,
      },
      grid: {
        show: false,
      },
      xaxis: {
        categories: [
          'India',
          'United States',
          'China',
          'Indonesia',
          'Russia',
          'Bangladesh',
          'Canada',
          'Brazil',
          'Vietnam',
          'UK',
        ],
      },
    };
  }

  async ngOnInit() {
    this.sessionPatientInfo = sessionStorage.getItem('itemPatientInfo');
    //itemPatientInfo = an
    this.itemPatientInfo = JSON.parse(this.sessionPatientInfo);

    //console.log('itemPatientInfo : ', this.itemPatientInfo);
    //ken writing
    this.listActivity();
    this.listEvaluate();
    //Patient info
    this.getNursePatient();
    this.getDoctor();
  }


  async getNursenote(id: any) {
    try {
      const respone = await this.nurseService.getNurseNote(id);
      const responseData: any = respone.data;
      let data: any = responseData;
      console.log('Nurse note:', data);
      this.NurseNote = data.data;
    } catch (error: any) {
      console.log(error);
      this.notificationService.notificationError(
        'คำชี้แจ้ง',
        'พบข้อผิดพลาด..' + error,
        'top'
      );
    }
  }
  async listActivity() {
    try {
      const respone = await this.nurseService.getActivity();
      this.itemActivityInfo = respone.data.data;
      // console.log(respone);
    } catch (error: any) {}
  }

  async listEvaluate() {
    try {
      const respone = await this.nurseService.getEvaluate();
      this.itemEvaluate = respone.data.data;
      // console.log(respone);
    } catch (error: any) {}
  }
  //แสดง บันทึกทางการพยาบาล nurse note
  showModalOrder(): void {
    this.isVisible = true;
  }
  showModalConsult(): void {
    this.isVisible = true;
  }
  showModal(): void {
    this.isVisible = true;
  }

  handleOk(): void {
    // console.log('Button ok clicked!');
    this.saveNurseNote();
    this.isVisible = false;
  }

  handleCancel(): void {
    // console.log('Button cancel clicked!');
    this.isVisible = false;
  }

  showModalVS(): void {
    this.isVisibleVS = true;
  }
  //แสดง modal vital sign note
  handleOkVS(): void {
    // console.log('Button ok clicked!');
    this.saveNurseVitalsign();
    this.isVisibleVS = false;
  }

  handleCancelVS(): void {
    // console.log('Button cancel clicked!');
    this.isVisibleVS = false;
  }
  showModalVSEdit(): void {
    this.isVisibleVSEdit = true;
  }
  //แสดง modal vital sign note
  handleOkVSEdit(): void {
    // console.log('Button ok clicked!');
    this.saveNurseVitalsign();
    this.isVisibleVSEdit = false;
  }

  handleCancelVSEdit(): void {
    // console.log('Button cancel clicked!');
    this.isVisibleVSEdit = false;
  }

  async getActivity(i: any, e: any) {
    console.log(i, ' : ', e);

    if (e.srcElement.checked) {
      this.activity_checked.push(i);
    } else {
      this.removeItemArray(this.activity_checked, i);
    }
    console.log(this.activity_checked);
  }

  async getEvaluate(i: any, e: any) {
    console.log(i, ' : ', e);

    if (e.srcElement.checked) {
      this.evaluate_checked.push(i);
    } else {
      this.removeItemArray(this.evaluate_checked, i);
    }
    console.log(this.evaluate_checked);
  }

  removeItemArray(array: any, item: any) {
    for (var i in array) {
      if (array[i] == item) {
        array.splice(i, 1);
        break;
      }
    }
  }

  async saveNurseNote() {
    let date: Date = new Date();
    // console.log(date);

    let data: any = {
      nurse_note_date: '2023-08-06',
      nurse_note_time: '12:22:00',
      problem_list: [this.problem_list],
      activity: this.activity_checked,
      evaluate: this.evaluate_checked,
      admit_id: this.itemPatientInfo.id,
      create_date: '2023-08-06',
      create_by: sessionStorage.getItem('userID'),
      modify_date: '',
      modify_by: sessionStorage.getItem('userID'),
      is_active: true,
      item_used: [{}],
    };
    // console.log(data);
    try {
      if (this.activity_checked.length || this.evaluate_checked.length) {
        const respone = await this.nurseService.saveNurseNote(data);
        // console.log(respone);
      }
    } catch (error: any) {}
  }
  async saveNurseVitalsign() {
    let date: Date = new Date();
    console.log('Vigtalsign วันที่: ' + date);

    let data: any = {
      admit_id: this.getadmit_id,
      vital_sign_date: '2023-12-20',
      vital_sign_time: '10:08:43',
      body_temperature: this.bt,
      pulse_rate: this.pr,
      respiratory_rate: '15',
      systolic_blood_pressure: '12',
      diatolic_blood_pressure: '12',
      intake_oral_fluid: this.oralfluide,
      intake_penterate: this.parenterat,
      intake_medicine: '23',
      outtake_urine: this.urineout,
      outtake_emesis: this.emesis,
      outtake_drainage: this.drainage,
      outtake_aspiration: this.aspiration,
      outtake_lochia: '23',
      stools: this.stools,
      urine: this.urine,
      pain_score: this.painscore,
      oxygen_sat: '43',
      body_weight: this.bw,
      create_at: '2023-12-23 21:39:00',
      create_by: this.getadmit_id,
      modify_at: '',
      modify_by: this.getadmit_id,
    };
    console.log('Vitalsign ดึงข้อมูล: ' + data);
    try {
      const response = await this.nurseService.saveNurseVitalSign(data);
      console.log('ken' + response);
      if (response.status === 200) {
        this.hideSpinner();
        this.notificationService.notificationSuccess(
          'คำชี้แจ้ง',
          'บันทึกสำเร็จ..',
          'top'
        );
        this.isSaved = true;
        setTimeout(() => {
          this.navigateNurse();
        }, 2000);
      }
    } catch (error) {
      console.log(error);
      this.hideSpinner();
      this.notificationService.notificationError(
        'คำชี้แจ้ง',
        'พบข้อผิดพลาด..' + error,
        'top'
      );
    }
  }
  async navigateNurse() {
    console.log('บันทึกสำเร็จ');
    this.router.navigate(['/nurse/patient-list/nurse-note']);
  }
  getadmit_id: any;
  async getNursePatient() {
    try {
      const response = await this.nurseService.getNursePatient(
        this.itemPatientInfo
      );
      this.getadmit_id = response.data.data[0].admit_id;
      this.address = response.data.data[0].address;
      this.admit_id = response.data.data[0].admit_id;
      this.age = response.data.data[0].age;
      this.an = response.data.data[0].an;
      this.blood_group = response.data.data[0].blood_group;
      this.cid = response.data.data[0].cid;
      this.citizenship = response.data.data[0].citizenship;
      this.create_by = response.data.data[0].create_by;
      this.create_date = response.data.data[0].create_date;
      this.dob = response.data.data[0].dob;
      this.fname = response.data.data[0].fname;
      this.gender = response.data.data[0].gender;
      this.hn = response.data.data[0].hn;
      this.id = response.data.data[0].id;
      this.inscl = response.data.data[0].inscl;
      this.inscl_name = response.data.data[0].inscl_name;
      this.insurance_id = response.data.data[0].insurance_id;
      this.is_active = response.data.data[0].is_active;
      this.is_pregnant = response.data.data[0].is_pregnant;
      this.lname = response.data.data[0].lname;
      this.married_status = response.data.data[0].married_status;
      this.modify_by = response.data.data[0].modify_by;
      this.modify_date = response.data.data[0].modify_date;
      this.nationality = response.data.data[0].nationality;
      this.occupation = response.data.data[0].occupation;
      this.phone = response.data.data[0].phone;
      this.reference_person_address =
        response.data.data[0].reference_person_address;
      this.reference_person_name = response.data.data[0].reference_person_name;
      this.reference_person_phone =
        response.data.data[0].reference_person_phone;
      this.reference_person_relationship =
        response.data.data[0].reference_person_relationship;
      this.religion = response.data.data[0].religion;
      this.title = response.data.data[0].title;

      //ken ดึงข้อมูล เตียงมาแสดง
      this.getPatientdataadmit(this.admit_id);

      //ดึงข้อมูลมาใส่ตัวแปล

      // //tab Info
      // await this.getNurseInfo();
      // //tab Note
      // await this.getNursenote(this.admit_id);
      // //tab V/S
      // await this.getNurseVitalSign(this.admit_id);
      // //tab Doctor Order
      // await this.getOrder(this.admit_id);
      // console.log('NuPatient',response);
    } catch (error) {}
  }
  async getPatientdataadmit(id: any) {
    try {
      const response = await this.nurseService.getDatabedanddoc(id);
      for (let i of this.doctors) {
        // console.log("loop docrot iiii",i);
        if (i.user_id == response.data.data[0].doctor_id) {
          // console.log("loop docrot",i);
          this.doctorname = i.title + i.fname + ' ' + i.lname;
          // console.log("ชื่อหมด:",this.doctorname);
        } else {
          console.log('หาหมอไม่เจอ');
        }
      }

      this.topbed = response.data.data[0].bed_number;
      // console.log('getDatabedanddoc:',response);
    } catch (error) {}
  }
  //หาหมอ
  async getDoctor() {
    console.log('getDoctor');
    try {
      const response: AxiosResponse = await this.libService.getDoctor();
      const responseData: any = response.data;
      const data: any = responseData.data;
      this.doctors = data;
      console.log('getDoctor : ', this.doctors);
    } catch (error: any) {
      console.log(error);
      this.notificationService.notificationError(
        'คำชี้แจ้ง',
        'พบข้อผิดพลาด..' + error,
        'top'
      );
    }
  }

  async getNurseInfo() {
    try {
      const response = await this.nurseService.getNurseinfo(this.getadmit_id);
      console.log('admit_id:', response);
      this.body_height = response.data.data[0].body_height;
      this.body_temperature = response.data.data[0].body_temperature;
      this.body_weight = response.data.data[0].body_weight;
      this.chief_complaint = response.data.data[0].chief_complaint;
      this.diatolic_blood_pressure =
        response.data.data[0].diatolic_blood_pressure;
      this.eye_score = response.data.data[0].eye_score;
      this.movement_score = response.data.data[0].movement_score;
      this.oxygen_sat = response.data.data[0].oxygen_sat;
      this.past_history = response.data.data[0].past_history;
      this.physical_exam = response.data.data[0].physical_exam;
      this.present_illness = response.data.data[0].present_illness;
      this.pulse_rate = response.data.data[0].pulse_rate;
      this.respiratory_rate = response.data.data[0].respiratory_rate;
      this.systolic_blood_pressure =
        response.data.data[0].systolic_blood_pressure;
      this.verbal_score = response.data.data[0].verbal_score;
      this.waist = response.data.data[0].waist;
    } catch (error) {}
  }

  async getNurseVitalSign(id: any) {
    try {
      const respone = await this.nurseService.getNursevital_sign(id);
      console.log('v/s:', respone);
    } catch (error) {}
  }

  async getOrder(id: any) {
    try {
      const respone = await this.nurseService.getorder(id);
      const responseData: any = respone.data;
      let data: any = responseData.getAdmitID;
      console.log('doctor order:', data);
      this.doctorOrder = data;
    } catch (error: any) {
      console.log(error);
      this.notificationService.notificationError(
        'คำชี้แจ้ง',
        'พบข้อผิดพลาด..' + error,
        'top'
      );
    }
  }
  //ken
  hideSpinner() {
    setTimeout(() => {
      this.spinner.hide();
    }, 1000);
  }

  // Chart Colors Set
  private getChartColorsArray(colors: any) {
    colors = JSON.parse(colors);
    return colors.map(function (value: any) {
      var newValue = value.replace(' ', '');
      if (newValue.indexOf(',') === -1) {
        var color = getComputedStyle(document.documentElement).getPropertyValue(
          newValue
        );
        if (color) {
          color = color.replace(' ', '');
          return color;
        } else return newValue;
      } else {
        var val = value.split(',');
        if (val.length == 2) {
          var rgbaColor = getComputedStyle(
            document.documentElement
          ).getPropertyValue(val[0]);
          rgbaColor = 'rgba(' + rgbaColor + ',' + val[1] + ')';
          return rgbaColor;
        } else {
          return newValue;
        }
      }
    });
  }
}
