import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MainComponent } from './main/main.component';
import { ChangeWardComponent } from './change-ward/change-ward.component';
import {PatientListComponent} from './patient-list.component';
import {NurseNoteComponent} from './nurse-note/nurse-note.component';

const routes: Routes = [
  {
    path: '',
    component: PatientListComponent,
    data: {
      breadcrumb: 'ทะเบียนผู้ป่วยใน'
    },
    children: [
      {
        path: '',

        component: MainComponent,
        data: {
          breadcrumb: 'ทะเบียนผู้ป่วยใน'
        },
      },
      {
        path: 'change-ward',

        component: ChangeWardComponent,
        data: {
          breadcrumb: 'ย้ายตึกหรือเตียง'
        },
      },
      {
        path: 'nurse-note',

        component: NurseNoteComponent,
        data: {
          breadcrumb: 'บันทึกการพยาบาล'
        },
      },
    ]
  }
]

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PatientListRoutingModule { }
